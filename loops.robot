***Settings***
Library    SeleniumLibrary
Library     String


***Variables***
${browser}  chrome
${URL}  http://automationpractice.com/index.php


***Keywords***
Open Homepage
    Open Browser       ${URL}       ${browser}


***Test Cases***
003 Contenedores loops
    Open Homepage
    Set Global Variable     @{nombreDeContenedores}     //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a
    FOR    ${nombreDeContenedor}    IN   ${nombreDeContenedores}
       Run Keyword If      '${nombreDeContenedor}'=='//*[@id="homefeatured"]/li[3]/div/div[2]/h5/a'    Exit For Loop
       Click Element       xpath=${nombreDeContenedor}
       Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
       Click Element       xpath=//*[@id="header_logo"]/a/img
    END
    Close Browser
