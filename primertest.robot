***Settings***

Resource   resources.robot

Suite Setup     Open Session
Suite Teardown  Close Browser

***Test Cases***
S001 First Test Case
   Set Browser Implicit Wait    5
   Wait Until Element Is Visible    xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input
   Input Text       xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/Input       ${keyword}
   Click Element    xpath=/html/body/div[1]/div[2]/div/img
   Click Element       xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]
   Title Should Be      ${keyword} - Buscar con Google
   Page Should Contain      ${keyword}