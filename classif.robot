***Settings***
Library    SeleniumLibrary


***Variables***
${browser}  chrome
${URL}  http://automationpractice.com/index.php
${selection}    Other

***Keywords***
Select Women Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[1]/a
    Title Should Be     Women - My Store

Select Dresses Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[2]/a
    Title Should Be     Dresses - My Store


***Test Cases***
002 Class If Testing
    Open Browser     ${URL}        ${browser}
    Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img
    Run Keyword If      '${selection}'=='Women'     Select Women Option     Else    Select Dresses Option
    Close Browser